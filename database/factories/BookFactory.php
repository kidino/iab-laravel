<?php

namespace Database\Factories;

use App\Models\Book;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Book::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => Str::title( $this->faker->sentence() ),
            'price' => $this->faker->randomFloat(2, 10, 1000)
        ];
    }

    public function update_authors() {
        $books = Book::all();

        foreach($books as $book) {
            $book->author_id = rand(1, 50);
            $book->save();
        }

        echo "Book updated";
    }

}
