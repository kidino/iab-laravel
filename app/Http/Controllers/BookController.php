<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Author;
use Illuminate\Http\Request;

class BookController extends Controller
{
    // senarai buku
    public function index() {

        $data = [];

        $data['books'] = Book::orderByDesc('id')->paginate(10);

        return view('book.index', $data);
    }

    // borang untuk kemaskini buku
    public function details( $id ) {
        $data['book'] = Book::findOrFail($id);

        return view('book.details', $data);

    }

    // proses untuk kemaskini buku
    public function update( Request $request, $id ) {

        $book = Book::findOrFail($id);

        $validated_data = $request->validate([
            'title' => 'required|min:5|max:255',
            'price' => 'required|numeric'
        ]);

        $book->title = $request->input('title');
        $book->price = $request->input('price');
        $book->save();

        return redirect('buku/'. $id )->with('status', 'Book updated!');
    
    }

    // borang untuk buku baru
    public function create() {
        return view('book.create');
    }

    // proses untuk simpan buku baru
    public function store( Request $request ) {

        $validated_data = $request->validate([
            'title' => 'required|min:5|max:255',
            'price' => 'required|numeric'
        ]);

        // $book = new Book;
        // $book->title = $request->input('title');
        // $book->price = $request->input('price');
        // $book->save();

        $book = Book::create($validated_data);

        return redirect('buku/create')->with('status', 'Book ['.$book->id.'] '. $book->title .' Added!');

    }

    public function delete($id) {
        $book = Book::findOrFail($id);

        $id = $book->id;
        $title = $book->title;

        $book->delete();

        return redirect('buku')->with('deleted', "Book [$id] $title deleted!");
    }

    public function author( $id ) {
        $author = Author::findOrFail($id);
        //dd( $author->books );

        return view('book.author', [ 'author' => $author ]);
    }   
}
