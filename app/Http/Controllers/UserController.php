<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    // display user list
    public function index() {

        $users = User::paginate(10);
        return view('user.index', [ 'users' => $users ]);
    }

    // display new user form
    public function create() {

        $roles = Role::all();
        return view('user.create', [ 'roles' => $roles ]);
    }

    // process add new user
    public function store(Request $request) {

        // dd($request);

        $validated_data = $request->validate([
            'name' => 'required|min:5|max:100',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
        ]);

        $validated_data['password'] = Hash::make( $validated_data['password'] );

        $user = User::create( $validated_data );

        $user->roles()->sync($request->input('roles')); // array of roles ID

        return redirect('user')->with('success', 'User ['.$user->id.'] '. $user->name .' Added!');

    }

    public function details( $id ) {
        $user = User::findOrFail( $id );
        $roles = Role::all();
        return view('user.details', [ 'user' => $user, 'roles' => $roles ]);
    }

    public function update(Request $request, $id ) {
        $user = User::findOrFail( $id );

        $validated_data = $request->validate([
            'name' => 'required|min:5|max:100',
            'email' => 'required|email|unique:users,email,'.$id,
        ]);

        $user->name = $request->input('name');
        $user->email = $request->input('email');

        $user->save();
        $user->roles()->sync($request->input('roles')); // array of roles ID

        return redirect('user/'.$id)->with('success', 'User has been updated!');

    }
    
}
