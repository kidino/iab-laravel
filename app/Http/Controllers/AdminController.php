<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{

    // protected page
    public function index() {
        if (!Auth::check()) {
            return redirect('admin/login');
        }

        echo "this page is protected<br>";
        echo "<a href='/user'>Go to user management</a>";
    }

    // display login page
    public function login_page() {
        return view ('admin.login_page');
    }

    // process login request
    public function login_process( Request $request ) {
        if (Auth::attempt([
            'email' => $request->input('email'), 
            'password' => $request->input('password')
            ])) {
                $request->session()->regenerate();
                return redirect()->intended('admin');
            } 
        return redirect('admin/login')->with('error', "Invalid username or password.");
    }

    // logout
    public function logout() {
        Auth::logout();
        return redirect('admin/login');
    }
}
