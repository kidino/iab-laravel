@extends('layout.main')

@section('content')

<div class="container">
    <div class="row">
        <div class="col col-md-8 offset-md-2 col-lg-4 offset-lg-4 ">

            <div class="bg-white login_form p-5 shadow rounded ">

                <h1>Login</h1>
@if (session('error'))
<div class="alert alert-danger">
    {{ session('error') }}
</div>
@endif
                <form method="post" action="{{ route('admin.login') }}" >
                    @csrf 
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" name="password">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>


        </div>
    </div>
</div>


@endsection