@extends('layout.main')

@section('content')

@if (session('status'))
<div class="alert alert-success">
    {{ session('status') }}
</div>
@endif

<h5><a href="/buku">&laquo; Back</a> &nbsp; Maklumat Buku</h5>
<h1>{{ $book->title }}</h1>

@if ($errors->any())
    <div class="alert alert-danger">
       Please fix your input data
    </div>
@endif


<form action="{{ route('book.details', [ 'id' => $book->id ]) }}" method="post">
    @csrf
    @method('put')

    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Title</label>
        <input type="text" class="form-control @error('title') is-invalid @enderror" value="{{ old('title', $book->title) }}" id="exampleInputEmail1" name="title">
        @error('title') <div class="invalid-feedback"> {{$message}} </div> @enderror
    </div>
    <div class="mb-3">
        <label for="exampleInputPassword1" class="form-label">Price</label>
        <input type="text" class="form-control @error('price') is-invalid @enderror" value="{{ old('price', $book->price) }}" id="exampleInputPassword1" name="price">
        @error('price') <div class="invalid-feedback"> {{$message}} </div> @enderror
    </div>
    <button type="submit" class="btn btn-primary">Save</button>

</form>
@endsection