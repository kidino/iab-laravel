@extends('layout.main')

@section('content')

@if (session('status'))
<div class="alert alert-success">
    {{ session('status') }}
</div>
@endif

<h1><a href="/buku">&laquo; Back</a> &nbsp; Tambah Buku Baru</h1>

@if ($errors->any())
    <div class="alert alert-danger">
       Please fix your input data
    </div>
@endif

<form action="{{ route('book.store') }}" method="post">
    @csrf

    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Title</label>
        <input type="text" class="form-control @error('title') is-invalid @enderror" id="exampleInputEmail1" name="title" value="{{old('title')}}">
        @error('title') <div class="invalid-feedback"> {{$message}} </div> @enderror
    </div>
    <div class="mb-3">
        <label for="exampleInputPassword1" class="form-label">Price</label>
        <input type="text" class="form-control  @error('price') is-invalid @enderror" id="exampleInputPassword1" name="price" value="{{old('price')}}">
        @error('price') <div class="invalid-feedback"> {{$message}} </div> @enderror
    </div>
    <button type="submit" class="btn btn-primary">Save</button>

</form>
@endsection