@extends('layout.main')

@section('content')

<h5><a href="/buku">&laquo; Back</a> &nbsp; Buku-buku Penulis</h5>
<h1>{{ $author->name }}</h1>

<table class="table table-striped">
    <thead>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Price</th>
            <!-- <th> </th>
            <th> </th> -->
        </tr>
    </thead>
    <thbody>


        @foreach( $author->books as $book)
        <tr>
            <td>{{ $book->id }}</td>
            <td>
                <a href="{{ route('book.details', [ 'id' => $book->id ] ) }}">
                {{ $book->title }}
                </a></td>
            <td>RM {{ $book->price }}</td>
            <!-- <td>
                <a href="{{ route('book.details', [ 'id' => $book->id ]) }}" class="btn btn-primary btn-sm">EDIT</a>
            </td>
            <td>
                <form action="{{ route('book.details', [ 'id' => $book->id ]); }}" method="post"
                    onsubmit="return confirm('Are you sure you want to delete this book?')">
                    @csrf
                    @method('delete')

                    <button class="btn btn-danger btn-sm">DELETE</button>
                </form>
            </td> -->
        </tr>

        @endforeach

    </tbody>
</table>


@endsection