<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Book Listing</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous">
    </script>
</head>

<body>
    <div class="container">
        <div class="row mt-5">
            <div class="col">

@if (session('deleted'))
<div class="alert alert-danger">
    {{ session('deleted') }}
</div>
@endif

                <a href="{{ route('book.create') }}" class="btn btn-primary float-end">Add New</a>
                <h1>Senarai Buku</h1>

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Price</th>
                            <th>Author</th>
                            <th> </th>
                            <th> </th>
                        </tr>
                    </thead>
                    <thbody>

                        @foreach( $books as $book) 
                        <tr>
                            <td>{{ $book->id }}</td>
                            <td>{{ $book->title }}</td>
                            <td>RM {{ $book->price }}</td>
                            <td>
                            <a href="{{ route('author.details', [ 'id' => $book->author_id ]) }}">
                            {{ $book->author->name }}
                            </a>
                            </td>
                            <td>
                                <a href="{{ route('book.details', [ 'id' => $book->id ]) }}" class="btn btn-primary btn-sm">EDIT</a>
                            </td>
                            <td>
                                <form action="{{ route('book.details', [ 'id' => $book->id ]); }}" 
                                    method="post" 
                                    onsubmit="return confirm('Are you sure you want to delete this book?')">
                                    @csrf 
                                    @method('delete')

                                    <button class="btn btn-danger btn-sm">DELETE</button>
                                </form>
                            </td>
                        </tr>

                        @endforeach

                    </thbody>
                </table>

                {{ $books->links() }}

            </div>
        </div>
    </div>

</body>

</html>