@extends('layout.main')

@section('content')

<h5><a href="/user">&laquo; Back</a> Maklumat Pengguna</h5>
<h1>{{ $user->name }}</h1>

@if (session('success'))
<div class="alert alert-success">
    {{ session('success') }}
</div>
@endif

<form action="{{ route('user.details', [ 'id' => $user->id ] ) }}" method="post">
@csrf
@method('put')

<div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Name</label>
    <input type="text" class="form-control @error('name') is-invalid @enderror" id="exampleInputEmail1" name="name" value="{{old('name', $user->name)}}">
    @error('name') <div class="invalid-feedback"> {{$message}} </div> @enderror
</div>

<div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Email</label>
    <input type="email" class="form-control @error('email') is-invalid @enderror" id="exampleInputEmail1" name="email" value="{{old('email', $user->email)}}">
    @error('email') <div class="invalid-feedback"> {{$message}} </div> @enderror
</div>

@foreach($roles as $role )
<div class="form-check">

    @php
        $checked = '';
        foreach($user->roles as $user_role) {
            if ($user_role->id == $role->id) {
                $checked = 'checked';
                break;
            }
        }
    @endphp
  <input name="roles[]" {{$checked}} class="form-check-input" type="checkbox" value="{{ $role->id }}" >

  <label class="form-check-label" for="flexCheckDefault">
    {{$role->name}}
  </label>
</div>
@endforeach

<button type="submit" class="btn btn-primary mt-3">Save</button>


</form>

@endsection