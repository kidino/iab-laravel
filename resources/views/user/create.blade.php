@extends('layout.main')

@section('content')

<h1>Add New User</h1>

<form action="{{ route('user') }}" method="post">

@csrf

<div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Name</label>
    <input type="text" class="form-control @error('name') is-invalid @enderror" id="exampleInputEmail1" name="name" value="{{old('name')}}">
    @error('name') <div class="invalid-feedback"> {{$message}} </div> @enderror
</div>

<div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Email</label>
    <input type="email" class="form-control @error('email') is-invalid @enderror" id="exampleInputEmail1" name="email" value="{{old('email')}}">
    @error('email') <div class="invalid-feedback"> {{$message}} </div> @enderror
</div>

<div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Password</label>
    <input type="password" class="form-control @error('password') is-invalid @enderror" id="exampleInputEmail1" name="password" value="{{old('password')}}">
    @error('password') <div class="invalid-feedback"> {{$message}} </div> @enderror
</div>

<div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Confirm Password</label>
    <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" id="exampleInputEmail1" name="password_confirmation" value="{{old('password_confirmation')}}">
    @error('password_confirmation') <div class="invalid-feedback"> {{$message}} </div> @enderror
</div>

@foreach($roles as $role )
<div class="form-check">
  <input name="roles[]" class="form-check-input" type="checkbox" value="{{ $role->id }}" >
  <label class="form-check-label" for="flexCheckDefault">
    {{$role->name}}
  </label>
</div>
@endforeach

<button type="submit" class="btn btn-primary">Save</button>


</form>

@endsection