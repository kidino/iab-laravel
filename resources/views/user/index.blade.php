@extends('layout.main')

@section('content')

@if (session('success'))
<div class="alert alert-success">
    {{ session('success') }}
</div>
@endif

    <form action="{{ route('user.logout') }}" method="post">
        @csrf
        <button class="btn btn-primary btn-sm">Logout</button>
    </form>

<a href="{{ route('user.create') }}" class="btn btn-primary float-end">Add New</a>
                <h1>User List</h1>

        <table class="table table-striped">

            <thead>
                <tr>
                    <th>ID</th>
                    <th>EMAIL</th>
                    <th>NAME</th>
                    <th></th>
                    <th></th>
                </tr>

            </thead>
            <tbody>
                @foreach($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->name }}</td>
                    <td><a href="{{ route('user.details', [ 'id' => $user->id ]) }}" class="btn btn-primary btn-sm">EDIT</a></td>
                    <td>
                        <form action="" method="post">
                            
                        </form>

                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>

        {{ $users->links() }}

@endsection 