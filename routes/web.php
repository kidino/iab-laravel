<?php

use App\Models\Book;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\WelcomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('hello');
});

Route::get('/hello', function () {
    return view('hello');
});

Route::get('/welcome', [WelcomeController::class, 'index']);

Route::get('/login', [LoginController::class, 'index']);

Route::post('/login', [LoginController::class, 'store'])->name('login.store');

// senarai buku
Route::get('/buku', [BookController::class, 'index'])->name('book.listing');

// proses simpan buku baru
Route::post('/buku', [BookController::class, 'store'])->name('book.store');

// form untuk buku baru
Route::get('/buku/create', [BookController::class, 'create'])->name('book.create');

// form untuk edit buku
Route::get('/buku/{id}', [BookController::class, 'details'])->name('book.details');

//proses untuk kemaskini buku
Route::put('/buku/{id}', [BookController::class, 'update']);

//proses untuk delete buku
Route::delete('/buku/{id}', [BookController::class, 'delete']);

// Route::get('random', function(){
//     $books = Book::all();

//     foreach($books as $book) {
//         $book->author_id = rand(1, 50);
//         $book->save();
//     }

//     echo "Book updated";
// });

Route::get('penulis/{id}', [ BookController::class, 'author' ])->name('author.details');

Route::get('admin/login', [ AdminController::class, 'login_page'])->name('admin.login');
Route::post('admin/login', [ AdminController::class, 'login_process']);

Route::get('admin', [ AdminController::class, 'index'])->name('admin.index');

Route::middleware('auth')->group(function(){
    Route::get('user', [ UserController::class, 'index' ])->name('user');
    Route::get('user/create', [ UserController::class, 'create' ])->name('user.create');
    Route::post('user', [ UserController::class, 'store' ]);
    Route::post('user/logout', [ AdminController::class, 'logout' ])->name('user.logout');    

    Route::get('/user/{id}', [ UserController::class, 'details' ])->name('user.details');
    Route::put('/user/{id}', [ UserController::class, 'update' ]);

});


